import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: FormScreen(),
    );
  }
}

class FormScreen extends StatefulWidget {
  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  final firebase = FirebaseFirestore.instance;
  String? name;
  String? surname;
  String? email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Card(
          margin: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Form(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      onChanged: (value) {
                        name = value;
                      },
                      decoration: InputDecoration(
                        labelText: 'name',
                      ),
                    ),
                    TextFormField(
                      onChanged: (value) {
                        surname = value;
                      },
                      decoration: InputDecoration(labelText: 'surname'),
                    ),
                    TextFormField(
                      onChanged: (value) {
                        email = value;
                      },
                      decoration: InputDecoration(labelText: 'email'),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text('*delete and update user by name.'),
                    ElevatedButton(
                      onPressed: () {
                        firebase.collection('profile').add(
                            {'name': name, 'surname': surname, 'email': email});
                      },
                      child: Text('Add'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        firebase
                            .collection('profile')
                            .doc('7BZyjUjjtSFCvdpZH1DY')
                            .update({'name': name});
                      },
                      child: const Text('Update Username'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        firebase
                            .collection('profile')
                            .doc('7BZyjUjjtSFCvdpZH1DY')
                            .delete();
                      },
                      child: const Text('Delete User'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        print(firebase
                            .collection('profile')
                            .doc('O9WSmIxVW4zKfKmjycLi')
                            .get());
                      },
                      child: Text('Read User'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
